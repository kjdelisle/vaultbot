FROM golang:1.12.1-stretch as builder
WORKDIR /vaultbot
COPY / /vaultbot/
RUN go get .
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o vaultbot .

FROM alpine:latest
WORKDIR /root/
COPY --from=builder /vaultbot/vaultbot .
RUN apk update && apk add ca-certificates && rm -rf /var/cache/apk/*
ENTRYPOINT ["./vaultbot"]